package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type LogPushGateway struct{}

func (self *LogPushGateway) SendPushNotification(telephone, body, payload string) error {
	log.Printf("new push notification: %s -> %s (payload: %s)\n", body, telephone, payload)
	return nil
}

type ParsePushGateway struct {
	AppId  string
	ApiKey string
}

func (self *ParsePushGateway) SendPushNotification(telephone, body, payload string) error {
	url := "https://api.parse.com/1/push"

	var query = struct {
		Where struct {
			Telephone string `json:"telephone"`
		} `json:"where"`
		Data struct {
			Alert   string `json:"alert"`
			Payload string `json:"payload"`
		} `json:"data"`
	}{}

	query.Where.Telephone = telephone
	query.Data.Alert = body
	query.Data.Payload = payload

	query_json, err := json.Marshal(query)
	if err != nil {
		log.Println("Error while crafting request body")
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewReader([]byte(query_json)))
	if err != nil {
		log.Println("ParsePushGateway.SendPushNotification: unable to create the request")
		return err
	}
	req.Header.Set("X-Parse-Application-Id", self.AppId)
	req.Header.Set("X-Parse-REST-API-Key", self.ApiKey)
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("ParsePushGateway.SendPushNotification: error in request")
		return err
	}

	if res.StatusCode != 200 {
		log.Println("Parse request: unexpected status code:", res.StatusCode)
		rbody, _ := ioutil.ReadAll(res.Body)
		log.Println(string(rbody))
	}

	defer res.Body.Close()

	return nil
}
