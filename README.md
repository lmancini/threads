# Threads

Implementation of a one-to-one mobile social platform.

![test badge](https://img.shields.io/shippable/56269a1c1895ca44741f111e/master.svg)

# Build

    go build

# Run

    # Mandatory configuration (Twilio specific)

    # Telephone number registered with Twilio
    export THREADS_NUMBER=<number>

    # Public URL for Twilio to notify call status
    export THREADS_PUBLIC_URL=<URL>

    # Optional configuration

    export THREADS_ADDR=address:port

    # If you want your deploy to send actual SMS
    export PLIVO_ID=<your plivo id>
    export PLIVO_TOKEN=<your plivo token>

    # If you want your deploy to send actual push notifications
    export PARSE_APP_ID=<your parse app id>
    export PARSE_API_KEY=<your parse api key>

    ./threads

# Test

    go test ./... -v
