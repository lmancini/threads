package threads

import (
	"log"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Thread struct {
	Id         int64  `json:"id,string"`
	Telephone1 string `json:"telephone1"`
	Telephone2 string `json:"telephone2"`
	Otp        uint32 `json:"otp,string"`
	Feedback   int    `json:"feedback"`
	Timestamp  int64  `json:"timestamp"`
}

// CanAcceptAFeedback returns true if it's still possible to leave feedback for
// this thread, false otherwise.
func (self *Thread) CanAcceptAFeedback() bool {

	// Any non-0 value means that a feedback was already left, and we don't
	// allow overwriting a feedback.
	if self.Feedback != 0 {
		return false
	}

	// After 5 minutes, the other end of the thread is lost.
	t := time.Unix(self.Timestamp, 0)
	if time.Now().After(t.Add(5 * time.Minute)) {
		return false
	}
	return true
}

// Update saves the new data for the modified thread, after running a sanity
// check (the thread must already be persisted in the storage).
func (self *Thread) Update() {
	if ThreadById(self.Id) == nil {
		panic("thread out of storage")
	}
	save()
}

type Profile struct {
	Telephone         string            `json:"telephone"`
	Data              map[string]string `json:"data"`
	PositiveFeedbacks int               `json:"positive_feedbacks"`
	NegativeFeedbacks int               `json:"negative_feedbacks"`
}

// TODO this variable can be changed according to the deploy
var profile_fields = []string{"time_slots", "second_chance"}

// Update saves the new data for the modified profile, after running a sanity
// check (the profile must already be persisted in the storage).
func (self *Profile) Update() {
	if ProfileByTelephone(self.Telephone) == nil {
		panic("profile out of storage")
	}
	save()
}

// SetData stores in the profile all values from the map arguments which are
// relevant for this profile (a value is considered relevant if its value is
// defined in `profile_fields`).
func (self *Profile) SetData(d map[string]string) {
	for _, key := range profile_fields {
		v, ok := d[key]
		if ok {
			self.Data[key] = v
		}
	}
	self.Update()
}

// CanBeDisturbed returns true if the profile owner is fine with being
// contacted at time `when`, false otherwise.
func (self *Profile) CanBeDisturbed(when time.Time) bool {
	time_slots := self.Data["time_slots"]
	if time_slots == "" {
		// default yes!
		return true
	}

	re := regexp.MustCompile("(\\d+(:\\d+)?)?-(\\d+(:\\d+)?)?")
	res := re.FindStringSubmatch(time_slots)
	if res == nil {
		return true
	}

	decode := func(t string) (minutes int) {
		if t == "" {
			return
		}
		res := strings.Split(t, ":")
		n, _ := strconv.ParseInt(res[0], 10, 32)
		minutes = int(n) * 60
		if len(res) == 2 {
			n, _ := strconv.ParseInt(res[1], 10, 32)
			minutes += int(n)
		}
		return
	}

	start := res[1]
	if start != "" {
		m := decode(start)
		if when.Hour()*60+when.Minute() < m {
			return false
		}
	}
	end := res[3]
	if end != "" {
		m := decode(start)
		if when.Hour()*60+when.Minute() > m {
			return false
		}
	}
	return true
}

type Session struct {
	Id        int64  `json:"id,string"`
	Telephone string `json:"telephone"`
	Client    string `json:"client"`
	Kind      string `json:"kind"`
	Lastseen  int64  `json:"lastseen"`

	Smscode  uint32 `json:"smscode"`
	Authcode int64  `json:"authcode"`
}

func (self *Session) Update() {
	if SessionById(self.Id) == nil {
		panic("session out of storage")
	}
	save()
}

func (self *Session) Ping() {
	self.Lastseen = time.Now().UnixNano()
}

func (self *Session) Validate(sms_code uint32) bool {
	if self.Smscode != sms_code {
		return false
	}
	self.Smscode = 0
	self.Authcode = NewUnguessableId()
	self.Ping()
	self.Update()
	return true
}

func (self *Session) IsAuthenticated() bool {
	return self.Authcode != -1
}

func (self *Session) IsFreshEnough() bool {
	d := time.Now().UnixNano() - self.Lastseen
	return self.Kind != "web" || (time.Duration(d)*time.Nanosecond).Minutes() <= 5
}

func NewThread(tel1, tel2 string) *Thread {
	t := Thread{
		Id:         NewUnguessableId(),
		Telephone1: tel1,
		Telephone2: tel2,
		Otp:        NewOTP(4),
		Feedback:   0,
		Timestamp:  time.Now().Unix(),
	}
	_threads = append(_threads, t)
	save()
	return ThreadById(t.Id)
}

func NewSession(tel, session_kind string) *Session {
	if session_kind != "physical" && session_kind != "web" {
		log.Panicln("unsupported kind of session")
	}
	p := ProfileByTelephone(tel)
	if p == nil {
		p := Profile{
			Telephone: tel,
			Data:      make(map[string]string),
		}
		_profiles = append(_profiles, p)
	}

	t := Session{
		Id:        NewUnguessableId(),
		Telephone: tel,
		Client:    NewUngussableClientName(),
		Kind:      session_kind,
		Lastseen:  time.Now().UnixNano(),

		Smscode:  NewOTP(6),
		Authcode: -1,
	}
	_sessions = append(_sessions, t)
	save()
	return SessionById(t.Id)
}

// ProfileByTelephone returns a Profile given its associated telephone number,
// or nil if it can't be found.
func ProfileByTelephone(tel string) *Profile {
	for ix := range _profiles {
		profile := &_profiles[ix]
		if profile.Telephone == tel {
			return profile
		}
	}
	return nil

}

func SessionById(id int64) *Session {
	for ix := range _sessions {
		session := &_sessions[ix]
		if session.Id == id {
			return session
		}
	}
	return nil
}

func SessionByAuthCode(code int64) *Session {
	for ix := range _sessions {
		session := &_sessions[ix]
		if session.Authcode == code {
			return session
		}
	}
	return nil
}

func SessionByClientName(name string) *Session {
	for ix := range _sessions {
		session := &_sessions[ix]
		if session.Client == name {
			return session
		}
	}
	return nil
}

// BestSessionByTelephoneNumber returns the most likely to work session for
// the given number
func BestSessionByTelephoneNumber(tel string) *Session {
	var sessions []*Session
	for ix := range _sessions {
		session := &_sessions[ix]
		if session.Telephone == tel && session.IsFreshEnough() {
			if session.Kind == "physical" {
				return session
			}
			sessions = append(sessions, session)
		}
	}
	if len(sessions) > 0 {
		return sessions[0]
	}
	return nil
}

func ThreadById(id int64) *Thread {
	for ix := range _threads {
		thread := &_threads[ix]
		if thread.Id == id {
			return thread
		}
	}
	return nil
}

func ThreadByOTP(tel string, otp uint32) *Thread {
	for ix := range _threads {
		thread := &_threads[ix]
		if thread.Telephone1 == tel && thread.Otp == otp {
			return thread
		}
	}
	return nil
}

func ThreadsByTelephone(tel string) []*Thread {
	var output []*Thread
	for ix := range _threads {
		thread := &_threads[ix]
		if thread.Telephone1 == tel || thread.Telephone2 == tel {
			output = append(output, thread)
		}
	}
	return output
}

func selectAPeer(tel string, filter func(*Session, *Profile) bool) *Session {
	indexes := rand.Perm(len(_sessions))
	for ix := range indexes {
		session := &_sessions[ix]
		if !session.IsAuthenticated() {
			continue
		}
		if session.Telephone == tel || !session.IsFreshEnough() {
			continue
		}
		profile := ProfileByTelephone(session.Telephone)
		if !profile.CanBeDisturbed(time.Now()) {
			continue
		}
		if filter != nil && !filter(session, profile) {
			continue
		}
		return session
	}
	return nil
}

// FindAPeer finds another user that can be called by the specified number
func FindAPeer(tel string) *Session {
	return selectAPeer(tel, nil)
}
