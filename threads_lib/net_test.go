package threads

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"
)

// FakeSmsGateway is a tracking SMS gateway for test purposes

type FakeSmsGateway struct {
	LastSms              string
	LastVerificationCode uint32
}

func (self *FakeSmsGateway) Send(from, to, msg string) error {
	self.LastSms = to
	return nil
}

func (self *FakeSmsGateway) SendVerificationCode(from, to string, code uint32) error {
	self.LastVerificationCode = code
	return nil
}

// FakePushGateway is a tracking push notification gateway for test purposes

type FakePushGateway struct {
	LastTelephone string
	LastBody      string
	LastPayload   string
}

func (self *FakePushGateway) SendPushNotification(telephone, body, payload string) error {
	self.LastTelephone = telephone
	self.LastBody = body
	self.LastPayload = payload
	return nil
}

type ThreadData struct {
	Id       string `json:"id"`
	Endpoint string `json:"endpoint"`
	Otp      string
}

type test_context struct {
	URL          string
	SMS          *FakeSmsGateway
	Push         *FakePushGateway
	Authcode     string
	ClientNumber string

	t          *testing.T
	session_id int64
	teardown   func()

	LastOnThreadFeedback struct {
		Feedback string
		Profile  *Profile
		Thread   *Thread
	}

	LastOnTwilioCallback struct {
		Status      string
		Sid         string
		Duration    uint64
		Thread      *Thread
		SessionType string
	}
}

func (self *test_context) Close() {
	self.teardown()
}

func (self *test_context) new_session(tel, kind string) string {
	body := struct {
		Telephone string `json:"telephone"`
		Kind      string `json:"kind"`
	}{tel, kind}

	r := self.post("/v1beta/me/sessions/", body, 200)
	resp := struct {
		Id     string `json:"id"`
		Client string `json:"client"`
	}{}
	err := json.Unmarshal(r, &resp)
	if err != nil {
		self.t.Fatal(err)
	}

	return resp.Id
}

func (self *test_context) session_sms_code(id string) uint32 {
	return self.SMS.LastVerificationCode
}

func (self *test_context) validate_session(id string, code uint32) string {
	validate := struct {
		Smscode uint32 `json:"smscode"`
	}{code}

	r := self.post("/v1beta/me/sessions/"+id, validate, 200)
	resp := struct {
		Authcode string `json:"authcode"`
	}{}
	err := json.Unmarshal(r, &resp)
	if err != nil {
		self.t.Fatal(err)
	}
	return resp.Authcode
}

func (self *test_context) InternalSession() *Session {
	return SessionById(self.session_id)
}

func (self *test_context) NewSession(tel, kind string) string {
	session_id := self.new_session(tel, kind)
	smscode := self.session_sms_code(session_id)
	return self.validate_session(session_id, smscode)
}

func (self *test_context) Authenticate() {
	self.Authcode = ""

	session_id := self.new_session(self.ClientNumber, "web")
	smscode := self.session_sms_code(session_id)
	self.Authcode = self.validate_session(session_id, smscode)

	_id, err := strconv.ParseInt(session_id, 10, 64)
	if err != nil {
		self.t.Fatal(err)
	}
	self.session_id = _id
}

func (self *test_context) NewThread() ThreadData {
	r := self.post("/v1beta/threads/", nil, 200)
	resp := ThreadData{}
	err := json.Unmarshal(r, &resp)
	if err != nil {
		self.t.Fatal(err)
	}
	pieces := strings.Split(resp.Endpoint, ",")
	endpoint_otp := pieces[len(pieces)-1]

	if endpoint_otp != resp.Otp {
		self.t.Fatal("OTPs in endpoint and response differ")
	}

	return resp
}

type ThreadSummary struct {
	Id        string `json:"id"`
	Timestamp string `json:"timestamp"`
	Feedback  string `json:"feedback"`
}

func (self *test_context) ThreadsList() []ThreadSummary {
	r := self.get("/v1beta/threads/", 200)
	var resp []ThreadSummary
	err := json.Unmarshal(r, &resp)
	if err != nil {
		self.t.Fatal(err)
	}
	return resp
}

func (self *test_context) ResetLastOnThreadFeedback() {
	self.LastOnThreadFeedback.Feedback = ""
	self.LastOnThreadFeedback.Profile = nil
	self.LastOnThreadFeedback.Thread = nil
}

func (self *test_context) ResetLastOnTwilioCallback() {
	self.LastOnTwilioCallback.Status = ""
	self.LastOnTwilioCallback.Sid = ""
	self.LastOnTwilioCallback.Duration = 0
	self.LastOnTwilioCallback.Thread = nil
	self.LastOnTwilioCallback.SessionType = ""
}

func (self *test_context) request(method, urlStr string, payload interface{}, exp_code int) []byte {
	var body *bytes.Buffer

	if payload != nil {
		octets, err := json.Marshal(payload)
		if err != nil {
			self.t.Fatal(err)
		}
		body = bytes.NewBuffer(octets)
	} else {
		body = bytes.NewBuffer(nil)
	}

	req, err := http.NewRequest(method, urlStr, body)
	if err != nil {
		self.t.Fatal(err)
	}

	req.Header.Set("Content-Type", "application/json")
	if self.Authcode != "" {
		req.SetBasicAuth("", self.Authcode)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		self.t.Fatal(err)
	}

	defer resp.Body.Close()
	if resp.StatusCode != exp_code {
		self.t.Fatal(fmt.Sprintf("Expected a %d, got a %d instead\n", exp_code, resp.StatusCode))
	}

	rbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		self.t.Fatal(err)
	}

	return rbody
}

func (self *test_context) post(path string, payload interface{}, exp_code int) []byte {
	url := self.URL + path
	return self.request("POST", url, payload, exp_code)
}

func (self *test_context) put(path string, payload interface{}, exp_code int) []byte {
	url := self.URL + path
	return self.request("PUT", url, payload, exp_code)
}

func (self *test_context) head(path string, exp_code int) []byte {
	url := self.URL + path
	return self.request("HEAD", url, nil, exp_code)
}

func (self *test_context) get(path string, exp_code int) []byte {
	url := self.URL + path
	return self.request("GET", url, nil, exp_code)
}

const TEST_NUMBER = "+391337"

var build_net_app_config (func(ctx **test_context, sms SmsGateway, push PushGateway) NetConfig) = func(ctx **test_context, sms SmsGateway, push PushGateway) NetConfig {
	return NetConfig{
		PublicURL:       "http://localhost/",
		TelephoneNumber: TEST_NUMBER,
		SMS:             sms,
		Push:            push,
		OnThreadFeedback: func(feedback string, p *Profile, t *Thread) {
			(*ctx).LastOnThreadFeedback.Feedback = feedback
			(*ctx).LastOnThreadFeedback.Profile = p
			(*ctx).LastOnThreadFeedback.Thread = t
		},
		OnTwilioCallback: func(call_status, call_sid string, call_duration uint64, t *Thread, s string) {
			(*ctx).LastOnTwilioCallback.Status = call_status
			(*ctx).LastOnTwilioCallback.Sid = call_sid
			(*ctx).LastOnTwilioCallback.Duration = call_duration
			(*ctx).LastOnTwilioCallback.Thread = t
			(*ctx).LastOnTwilioCallback.SessionType = s
		},
	}
}

func setup(t *testing.T) *test_context {
	temp_storage, err := ioutil.TempFile("", "")
	if err != nil {
		log.Panicln(err)
	}
	InitStorage(temp_storage)

	sms_gateway := FakeSmsGateway{}
	push_gateway := FakePushGateway{}

	var ctx *test_context
	cfg := build_net_app_config(&ctx, &sms_gateway, &push_gateway)

	mux := NewWebMux(cfg)
	server := httptest.NewServer(mux)

	teardown := func() {
		server.Close()
		os.Remove(temp_storage.Name())
	}

	ctx = &test_context{
		URL:          server.URL,
		SMS:          &sms_gateway,
		Push:         &push_gateway,
		ClientNumber: "+39055123456789",
		t:            t,
		teardown:     teardown,
	}
	return ctx
}

// waitANanosecond is used in all tests where at least some time is expected to
// pass between two actions.
func waitANanosecond() {
	time.Sleep(1)
}

func TestSessionBasicApi(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
}

func TestSessionInvalidId(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	smscode := ctx.session_sms_code(ctx.new_session(ctx.ClientNumber, "web"))
	session_id := "42"

	validate := struct {
		Smscode uint32 `json:"smscode"`
	}{smscode}

	ctx.post("/v1beta/me/sessions/"+session_id, validate, 404)
}

func TestSessionInvalidSmsCode(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	session_id := ctx.new_session(ctx.ClientNumber, "web")
	smscode := ctx.session_sms_code(session_id)
	smscode += 1

	validate := struct {
		Smscode uint32 `json:"smscode"`
	}{smscode}

	ctx.post("/v1beta/me/sessions/"+session_id, validate, 404)
}

func TestClientCanHaveMultipleDistintSessions(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	session_id1 := ctx.new_session(ctx.ClientNumber, "web")
	smscode1 := ctx.session_sms_code(session_id1)
	authcode1 := ctx.validate_session(session_id1, smscode1)

	session_id2 := ctx.new_session(ctx.ClientNumber, "web")
	smscode2 := ctx.session_sms_code(session_id2)
	authcode2 := ctx.validate_session(session_id2, smscode2)

	if session_id1 == session_id2 {
		ctx.t.Fatal("same session id?")
	}

	if authcode1 == authcode2 {
		ctx.t.Fatal("same authentication code?")
	}
}

func TestExplicitKeepalive(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	t0 := ctx.InternalSession().Lastseen

	waitANanosecond()
	ctx.head("/v1beta/me", 200)
	t1 := ctx.InternalSession().Lastseen

	if t1 <= t0 {
		ctx.t.Fatal("session timestamp not updated")
	}
}

func TestImplicitKeepalive(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	t0 := ctx.InternalSession().Lastseen

	waitANanosecond()
	ctx.get("/v1beta/me", 200)
	t1 := ctx.InternalSession().Lastseen

	if t1 <= t0 {
		ctx.t.Fatal("session timestamp not updated")
	}
}

func TestUpdateProfile(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()

	body := struct {
		TimeSlots     string `json:"time_slots"`
		SecondChanche string `json:"second_chance"`
	}{"10-20", "foo"}
	ctx.put("/v1beta/me", body, 200)

	octets := ctx.get("/v1beta/me", 200)
	response := struct {
		Data map[string]string `json:"data"`
	}{}
	err := json.Unmarshal(octets, &response)
	if err != nil {
		ctx.t.Fatal("error decoding json.", err)
	}

	if response.Data["time_slots"] != "10-20" {
		ctx.t.Fatal("profile update fail. time_slots")
	}

	if response.Data["second_chance"] != "foo" {
		ctx.t.Fatal("profile update fail. second_chance")
	}
}

func TestUpdateProfileAFieldATime(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()

	response := struct {
		Data map[string]string `json:"data"`
	}{}

	body0 := struct {
		TimeSlots     string `json:"time_slots"`
		SecondChanche string `json:"second_chance"`
	}{"10-20", "foo"}
	ctx.put("/v1beta/me", body0, 200)

	body1 := struct {
		TimeSlots string `json:"time_slots"`
	}{"11-12"}
	ctx.put("/v1beta/me", body1, 200)

	octets := ctx.get("/v1beta/me", 200)
	err := json.Unmarshal(octets, &response)
	if err != nil {
		ctx.t.Fatal("error decoding json.", err)
	}

	if response.Data["time_slots"] != "11-12" {
		ctx.t.Fatal("profile update fail. time_slots")
	}

	if response.Data["second_chance"] != "foo" {
		ctx.t.Fatal("profile update fail. second_chance")
	}

	body2 := struct {
		SecondChanche string `json:"second_chance"`
	}{"bar"}
	ctx.put("/v1beta/me", body2, 200)

	response.Data = make(map[string]string)
	octets = ctx.get("/v1beta/me", 200)
	err = json.Unmarshal(octets, &response)
	if err != nil {
		ctx.t.Fatal("error decoding json.", err)
	}

	if response.Data["time_slots"] != "11-12" {
		ctx.t.Fatal("profile update fail. time_slots")
	}

	if response.Data["second_chance"] != "bar" {
		ctx.t.Fatal("profile update fail. second_chance")
	}
}

func TestAnonymousAccess(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.get("/v1beta/me", 401)
}

func TestCreateNewThread(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	if resp.Endpoint[:len(TEST_NUMBER)] != TEST_NUMBER {
		ctx.t.Fatal("invalid endpoint")
	}
}

func TestCreateNewThreadWithoutOtherUsers(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.post("/v1beta/threads/", nil, 404)
}

func TestCreateNewThreadWhenTheOtherIsDND(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	now := time.Now()

	// The profile owner is only fine to be contacted a hour and one minute
	// from now
	_profile := ProfileByTelephone("+390123")
	next_hour := strconv.FormatInt(int64(now.Hour()+1), 10)
	next_minute := strconv.FormatInt(int64(now.Minute()+1), 10)
	ts := next_hour + ":" + next_minute + "-"
	_profile.Data["time_slots"] = ts
	_profile.Update()

	ctx.post("/v1beta/threads/", nil, 404)
}

func TestCreateNewThreadWhenTheOtherIsDND_2(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	now := time.Now()
	if now.Hour() == 0 {
		t.Skip("don't try this at midnight :)")
	}

	// The profile owner is only fine to be contacted until hour and one minute
	// ago
	_profile := ProfileByTelephone("+390123")
	prev_hour := strconv.FormatInt(int64(now.Hour()-1), 10)
	prev_minute := strconv.FormatInt(int64(now.Minute()-1), 10)
	ts := "-" + prev_hour + ":" + prev_minute
	_profile.Data["time_slots"] = ts
	_profile.Update()

	ctx.post("/v1beta/threads/", nil, 404)
}

func TestRetrieveThreadsList(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()

	ctx.NewSession("+390123", "web")
	resp1 := ctx.NewThread()
	_profile := ProfileByTelephone("+390123")
	_profile.Data["time_slots"] = "0:0-0:0"
	_profile.Update()

	ctx.NewSession("+390456", "web")
	resp2 := ctx.NewThread()

	threads := ctx.ThreadsList()
	if len(threads) != 2 {
		ctx.t.Fatal("unexpected threads list")
	}
	if threads[0].Id != resp1.Id {
		ctx.t.Fatal("invalid order")
	}
	if threads[1].Id != resp2.Id {
		ctx.t.Fatal("invalid order")
	}
}

func options(t *testing.T, url string, exp_code int) http.Header {
	req, err := http.NewRequest("OPTIONS", url, nil)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != exp_code {
		t.Fatalf("request failed with %d expected %d", resp.StatusCode, exp_code)
	}

	defer resp.Body.Close()
	return resp.Header
}

func get(t *testing.T, url string, exp_code int) []byte {
	resp, err := http.Get(url)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != exp_code {
		t.Fatalf("request failed with %d expected %d", resp.StatusCode, exp_code)
	}

	defer resp.Body.Close()

	rbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	return rbody
}

func post(t *testing.T, url string, data url.Values, exp_code int) []byte {
	resp, err := http.PostForm(url, data)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != exp_code {
		t.Fatalf("request failed with %d expected %d", resp.StatusCode, exp_code)
	}

	defer resp.Body.Close()

	rbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	return rbody
}

func TestCallFromTwilio(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "physical")

	resp := ctx.NewThread()
	twilio := fmt.Sprintf("/v1beta/twilio_request?From=%s&Digits=%s", url.QueryEscape(ctx.ClientNumber), resp.Otp)
	octets := get(t, ctx.URL+twilio, 200)

	if !bytes.Contains(octets, []byte("<Dial")) {
		t.Error("Call with the right OTP is supposd to be accepted")
	}

	if !bytes.Contains(octets, []byte("+390123")) {
		t.Error("We're supposed to call the other endpoint")
	}
}

func TestCallFromTwilioWithAClientName(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	auth2 := ctx.NewSession("+390123", "web")
	_id, _ := strconv.ParseInt(auth2, 10, 64)

	resp := ctx.NewThread()
	client_name := ctx.InternalSession().Client
	twilio := fmt.Sprintf("/v1beta/twilio_request?From=%s&Digits=%s", "client:"+url.QueryEscape(client_name), resp.Otp)
	octets := get(t, ctx.URL+twilio, 200)

	if !bytes.Contains(octets, []byte("<Dial")) {
		t.Error("Call with the right OTP is supposd to be accepted")
	}

	_session := SessionByAuthCode(_id)
	if !bytes.Contains(octets, []byte(_session.Client)) {
		t.Error("We're supposed to call the other endpoint")
	}
}

func TestCallFromTwilioWithAClientNameAndAnOTPArgument(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	client_name := ctx.InternalSession().Client
	twilio := fmt.Sprintf("/v1beta/twilio_request?From=%s&Otp=%s", "client:"+url.QueryEscape(client_name), resp.Otp)
	get(t, ctx.URL+twilio, 200)
}

func TestCallFromTwilioWithAnInvalidClientName(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	twilio := fmt.Sprintf("/v1beta/twilio_request?From=client:1&Digits=%s", resp.Otp)
	get(t, ctx.URL+twilio, 400)
}

func TestCallFromTwilioWithWrongParameters(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	tpl := ctx.URL + "/v1beta/twilio_request?From=%s&Digits=%s"

	get(t, fmt.Sprintf(tpl, ctx.ClientNumber, "a"), 400)
	if !bytes.Contains(get(t, fmt.Sprintf(tpl, ctx.ClientNumber, "1"), 200), []byte("<Reject")) {
		t.Error("Call with wrong OTP is supposed to be rejected")
	}
	if !bytes.Contains(get(t, fmt.Sprintf(tpl, "a", resp.Otp), 200), []byte("<Reject")) {
		t.Error("Call with wrong number is supposed to be rejected")
	}
	if !bytes.Contains(get(t, fmt.Sprintf(tpl, "a", "1"), 200), []byte("<Reject")) {
		t.Error("Call should be rejected")
	}
}

func TestCallFromTwilioWithoutOtp(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "physical")

	ctx.NewThread()
	twilio := fmt.Sprintf("/v1beta/twilio_request?From=%s", url.QueryEscape(ctx.ClientNumber))
	octets := get(t, ctx.URL+twilio, 200)

	if !bytes.Contains(octets, []byte("<Dial")) {
		t.Error("Call with the right OTP is supposd to be accepted")
	}

	if !bytes.Contains(octets, []byte("+390123")) {
		t.Error("We're supposed to call the other endpoint")
	}
}

func TestCallFromTwilioWithoutOtpFromUnknownNumber(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	ctx.NewThread()
	twilio := "/v1beta/twilio_request?From=1"
	get(t, ctx.URL+twilio, 400)
}

func TestCallbackFromTwilio(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	values := url.Values{}
	values.Set("DialCallStatus", "foobar")
	values.Set("DialCallSid", "0xdeadbeef")
	values.Set("DialCallDuration", "1")

	twilio := fmt.Sprintf("/v1beta/twilio_callback?thread=%s&session_type=web", resp.Id)
	post(t, ctx.URL+twilio, values, 200)
}

func TestCallbackFromTwilioRunCustomizationCallback(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	values := url.Values{}
	values.Set("DialCallStatus", "foobar")
	values.Set("DialCallSid", "0xdeadbeef")
	values.Set("DialCallDuration", "1")

	twilio := fmt.Sprintf("/v1beta/twilio_callback?thread=%s&session_type=web", resp.Id)
	post(t, ctx.URL+twilio, values, 200)

	if ctx.LastOnTwilioCallback.Status != "foobar" {
		t.Fatal("twilio customization callback not run")
	}

	if ctx.LastOnTwilioCallback.Sid != "0xdeadbeef" {
		t.Fatal("twilio customization callback not run")
	}

	if ctx.LastOnTwilioCallback.Duration != 1 {
		t.Fatal("twilio customization callback not run")
	}

	if strconv.FormatInt(ctx.LastOnTwilioCallback.Thread.Id, 10) != resp.Id {
		t.Fatal("twilio customization callback not run")
	}

	if ctx.LastOnTwilioCallback.SessionType != "web" {
		t.Fatal("twilio customization callback not run")
	}
}

func TestCallbackFromTwilioWithUnknownOrInvalidThreadId(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	ctx.NewThread()
	values := url.Values{}
	values.Set("DialCallStatus", "foobar")
	values.Set("DialCallSid", "0xdeadbeef")
	values.Set("DialCallDuration", "1")

	twilio := fmt.Sprintf("/v1beta/twilio_callback?thread=0&session_type=web")
	post(t, ctx.URL+twilio, values, 404)

	twilio = fmt.Sprintf("/v1beta/twilio_callback?session_type=web")
	post(t, ctx.URL+twilio, values, 400)

	twilio = fmt.Sprintf("/v1beta/twilio_callback?thread=a&session_type=web")
	post(t, ctx.URL+twilio, values, 400)
}

func TestCallbackFromTwilioWithInvalidSessionType(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	values := url.Values{}
	values.Set("DialCallStatus", "foobar")
	values.Set("DialCallSid", "0xdeadbeef")
	values.Set("DialCallDuration", "1")

	twilio := fmt.Sprintf("/v1beta/twilio_callback?thread=%s&session_type=xxx", resp.Id)
	post(t, ctx.URL+twilio, values, 400)
}

func TestCallbackFromTwilioWithInvalidOrMissingDuration(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()
	twilio := fmt.Sprintf("/v1beta/twilio_callback?thread=%s&session_type=web", resp.Id)

	values := url.Values{}
	values.Set("DialCallStatus", "foobar")
	values.Set("DialCallSid", "0xdeadbeef")
	// an empty duration is ok
	post(t, ctx.URL+twilio, values, 200)

	// a non numeric duration is not ok
	values.Set("DialCallDuration", "a")
	post(t, ctx.URL+twilio, values, 400)
}

type _Feedback struct {
	Feedback string `json:"feedback"`
}

func TestThreadFeedback(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	other_code := ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()

	ctx.Authcode = other_code
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"+1"}, 200)

	_id, err := strconv.ParseInt(resp.Id, 10, 64)
	if err != nil {
		t.Fatal(err)
	}
	thread := ThreadById(_id)
	if thread.Feedback != 1 {
		t.Fatal("thread not updated")
	}
}

func TestOnlyTheRecipientCanLeaveAFeedback(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()

	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"+1"}, 400)
}

func TestInvalidFeedbackValue(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	other_code := ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()

	ctx.Authcode = other_code
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"a"}, 400)
}

func TestCannotLeaveFeedbackOnInvalidThread(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	other_code := ctx.NewSession("+390123", "web")

	ctx.NewThread()

	ctx.Authcode = other_code
	ctx.put("/v1beta/threads/1", _Feedback{"+1"}, 404)
}

func TestWhenANonZeroFeedbackIsLeavedCannotBeUpdated(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	other_code := ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()

	ctx.Authcode = other_code
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"0"}, 200)
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"0"}, 200)
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"0"}, 200)
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"-1"}, 200)
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"+1"}, 400)
}

func TestAFeedbackCanBeLeavedWithin5Minutes(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	other_code := ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()

	ctx.Authcode = other_code
	_id, err := strconv.ParseInt(resp.Id, 10, 64)
	if err != nil {
		t.Fatal(err)
	}
	thread := ThreadById(_id)
	thread.Timestamp = time.Now().Add(-6 * time.Minute).Unix()
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"0"}, 400)
}

func TestLeavingFeedbackRunCustomizationCallback(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	other_code := ctx.NewSession("+390123", "web")

	resp := ctx.NewThread()

	ctx.Authcode = other_code
	ctx.put("/v1beta/threads/"+resp.Id, _Feedback{"0"}, 200)

	if ctx.LastOnThreadFeedback.Feedback != "0" || ctx.LastOnThreadFeedback.Profile == nil || ctx.LastOnThreadFeedback.Profile.Telephone != "+390123" {
		t.Fatal("callback not called")
	}
}

// TestThreadOffline tests that a web client that has been last seen more than
// a specific amount of time is not eligible for the creation of a thread
func TestThreadOffline(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	ctx.Authenticate()
	auth2 := ctx.NewSession("+390123", "web")

	_id, err := strconv.ParseInt(auth2, 10, 64)
	if err != nil {
		t.Fatal(err)
	}
	_session := SessionByAuthCode(_id)
	_session.Lastseen -= int64(6 * time.Minute)

	ctx.post("/v1beta/threads/", nil, 404)
}

func TestBasicCORSSupport(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	headers := options(t, ctx.URL+"/", 200)
	if headers.Get("Access-Control-Allow-Origin") != "*" {
		t.Fatal("Access-Control-Allow-Origin missing")
	}
	if headers.Get("Access-Control-Allow-Methods") != "GET,POST,PUT,HEAD" {
		t.Fatal("Access-Control-Allow-Methods invalid")
	}
	if headers.Get("Access-Control-Allow-Headers") != "Accept,Authorization,Origin,Content-Type" {
		t.Fatal("Access-Control-Allow-Headers invalid")
	}
}

// TestMalformedAuthorization exercises the code that parse and validate the
// Authorization Header
func TestMalformedAuthorization(t *testing.T) {
	ctx := setup(t)
	defer ctx.Close()

	f := func(val string) {
		u := ctx.URL + "/v1beta/me"
		req, err := http.NewRequest("HEAD", u, nil)
		if err != nil {
			t.Fatal(err)
		}

		req.Header.Set("Authorization", val)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}

		defer resp.Body.Close()
		if resp.StatusCode != 401 {
			t.Fatal(fmt.Sprintf("Expected a 401, got a %d instead\n", resp.StatusCode))
		}
	}
	// no space
	f("BasicX")

	// invalid case
	f("BASIC X")

	// invalid base64
	f("Basic X")

	// invalid number of elements
	f("Basic " + base64.StdEncoding.EncodeToString([]byte("X")))

	// invalid username (must be a number)
	f("Basic " + base64.StdEncoding.EncodeToString([]byte(":X")))

	// invalid username (must be a valid auth code)
	f("Basic " + base64.StdEncoding.EncodeToString([]byte(":1")))
}
