package threads

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/zenazn/goji/web"
	"github.com/zenazn/goji/web/middleware"
)

type serverError struct {
	code    int
	Message string `json:"message"`
}

func decode_json(value interface{}, w http.ResponseWriter, r *http.Request) error {
	err := json.NewDecoder(r.Body).Decode(value)
	if err != nil {
		send_error(serverError{400, "Error decoding the request body"}, w)
		return err
	}
	return nil
}

func encode_json(value interface{}, w http.ResponseWriter) error {
	bytes, err := json.Marshal(value)
	if err != nil {
		log.Panicf("cannot encode an object to json: %#v", value)
	}

	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(bytes)
	return err
}

func send_error(e serverError, w http.ResponseWriter) error {
	log.Printf("Send error: %#v\n", e)
	w.WriteHeader(e.code)
	return encode_json(e, w)
}

type SmsGateway interface {
	Send(from, to, msg string) error
	SendVerificationCode(from, to string, code uint32) error
}

type PushGateway interface {
	SendPushNotification(telephone, body, payload string) error
}

type NetConfig struct {
	PublicURL        string
	TelephoneNumber  string
	SMS              SmsGateway
	Push             PushGateway
	OnThreadFeedback func(feedback string, profile *Profile, thread *Thread)
	OnTwilioCallback func(call_status, call_sid string, call_duration uint64, thread *Thread, session_type string)
}

type NetApp struct {
	config NetConfig

	// pending_threads is a per-app state that map a thelephone number with a
	// thread id.
	// Is used as a workaround for the clients that can't sent the DTMF (our
	// OTP) to confirm a thread.
	pending_threads map[string]int64
}

type newSessionRequest struct {
	Telephone string `json:"telephone"`
	Kind      string `json:"kind"`
}

type newSessionResponse struct {
	SessionId int64  `json:"id,string"`
	Client    string `json:"client"`
}

func (self *NetApp) reachable_url(path string) string {
	return self.config.PublicURL + path
}

func (self *NetApp) create_new_session(c web.C, w http.ResponseWriter, r *http.Request) {
	req := newSessionRequest{}
	err := decode_json(&req, w, r)
	if err != nil {
		return
	}

	// TODO: validate telephone number; currently garbage is accepted.

	// We have a telephone number. Prepare a full Session struct, and initiate
	// SMS-handshaking with the user
	session := NewSession(req.Telephone, req.Kind)

	// Send an SMS to the user giving her the verification code
	self.config.SMS.SendVerificationCode(self.config.TelephoneNumber, session.Telephone, session.Smscode)

	resp := newSessionResponse{
		Client:    session.Client,
		SessionId: session.Id,
	}
	encode_json(resp, w)
}

type sessionsValidateRequest struct {
	Smscode uint32 `json:"smscode"`
}

type sessionsValidateResponse struct {
	Authcode int64 `json:"authcode,string"`
}

func (self *NetApp) validate_session(c web.C, w http.ResponseWriter, r *http.Request) {
	req := sessionsValidateRequest{}
	err := decode_json(&req, w, r)
	if err != nil {
		return
	}

	session_id, err := strconv.ParseInt(c.URLParams["session_id"], 10, 64)
	if err != nil {
		send_error(serverError{400, "Invalid session id"}, w)
		return
	}
	session := SessionById(session_id)
	if session == nil {
		http.NotFound(w, r)
		return
	}

	// We have a SMS code from a client willing to validate.
	// Check if indeed the SMS code matches with the one defined
	// for this session
	if !session.Validate(req.Smscode) {
		// No match. Return a 404 anyway, not to leak information
		// about this particular session id already being present
		http.NotFound(w, r)
		return
	}

	resp := sessionsValidateResponse{
		Authcode: session.Authcode,
	}
	encode_json(resp, w)
}

type userProfileResponse struct {
	Telephone string            `json:"telephone"`
	Data      map[string]string `json:"data"`
	Session   struct {
		Client string `json:"client"`
	}
}

func (self *NetApp) user_profile(c web.C, w http.ResponseWriter, r *http.Request) {
	session := c.Env["Session"].(*Session)
	profile := ProfileByTelephone(session.Telephone)
	resp := userProfileResponse{}
	resp.Telephone = profile.Telephone
	resp.Data = profile.Data
	resp.Session.Client = session.Client
	encode_json(resp, w)
}

func (self *NetApp) set_user_profile(c web.C, w http.ResponseWriter, r *http.Request) {
	session := c.Env["Session"].(*Session)
	profile := ProfileByTelephone(session.Telephone)

	req := make(map[string]string)
	err := decode_json(&req, w, r)
	if err != nil {
		return
	}

	profile.SetData(req)
}

func (self *NetApp) session_keepalive(c web.C, w http.ResponseWriter, r *http.Request) {
	// do nothing, the session is updated as a side effect of an
	// authenticated request
}

type listThreadResponse struct {
	Id        int64  `json:"id,string"`
	Timestamp int64  `json:"timestamp,string"`
	Feedback  string `json:"feedback"`
}

type ByTimestamp []listThreadResponse

func (self ByTimestamp) Len() int           { return len(self) }
func (self ByTimestamp) Swap(i, j int)      { self[i], self[j] = self[j], self[i] }
func (self ByTimestamp) Less(i, j int) bool { return self[i].Timestamp > self[j].Timestamp }

func (self *NetApp) list_threads(c web.C, w http.ResponseWriter, r *http.Request) {
	telephone := r.Header.Get("X-Caller-Telephone")
	threads := ThreadsByTelephone(telephone)

	var resp []listThreadResponse
	for _, thread := range threads {
		r := listThreadResponse{
			Id:        thread.Id,
			Timestamp: thread.Timestamp,
		}
		switch thread.Feedback {
		case 1:
			r.Feedback = "+1"
		case -1:
			r.Feedback = "-1"
		default:
			r.Feedback = "0"
		}
		resp = append(resp, r)
	}

	sort.Sort(ByTimestamp(resp))
	encode_json(resp, w)
}

type createThreadResponse struct {
	Id       int64  `json:"id,string"`
	Endpoint string `json:"endpoint"`
	Otp      uint32 `json:"otp,string"`
}

func (self *NetApp) create_new_thread(c web.C, w http.ResponseWriter, r *http.Request) {
	telephone := r.Header.Get("X-Caller-Telephone")

	// Now, we want to find another user to connect the caller to.
	session := FindAPeer(telephone)
	if session == nil {
		log.Printf("Cannot create a new thread. No peers available for %s\n", telephone)
		http.NotFound(w, r)
		return
	}

	thread := NewThread(telephone, session.Telephone)
	log.Printf("New thread between %s -> %s, Otp %d\n", telephone, session.Telephone, thread.Otp)

	// store the link between the user and the thread...
	self.pending_threads[telephone] = thread.Id
	// ...but only for a small amount of time (see the NetApp type)
	go func() {
		time.Sleep(5 * time.Minute)
		check := self.pending_threads[telephone]
		if check == thread.Id {
			delete(self.pending_threads, telephone)
		}
	}()

	resp := createThreadResponse{
		Id:       thread.Id,
		Endpoint: self.telephone_endpoint(thread),
		Otp:      thread.Otp,
	}
	encode_json(resp, w)
}

type threadFeedbackRequest struct {
	Feedback string `json:"feedback"`
}

func (self *NetApp) feedback_thread(c web.C, w http.ResponseWriter, r *http.Request) {
	req := threadFeedbackRequest{}
	err := decode_json(&req, w, r)
	if err != nil {
		return
	}

	thread_id, err := strconv.ParseInt(c.URLParams["thread_id"], 10, 64)
	if err != nil {
		send_error(serverError{400, "Invalid thread id"}, w)
		return
	}

	thread := ThreadById(thread_id)
	if thread == nil {
		http.NotFound(w, r)
		return
	}

	// only the receiver can leave a feedback
	telephone := r.Header.Get("X-Caller-Telephone")
	if thread.Telephone2 != telephone {
		send_error(serverError{400, "Only receiver can leave a feedback"}, w)
		return
	}

	if !thread.CanAcceptAFeedback() {
		send_error(serverError{400, "Can't leave feedback anymore"}, w)
		return
	}

	if req.Feedback != "-1" && req.Feedback != "0" && req.Feedback != "+1" {
		send_error(serverError{400, "Invalid request"}, w)
		return
	}

	profile := ProfileByTelephone(telephone)
	if req.Feedback == "0" {
		thread.Feedback = 0
		thread.Update()
	} else if req.Feedback == "+1" {
		thread.Feedback = 1
		thread.Update()

		profile.PositiveFeedbacks += 1
		profile.Update()
		log.Println("positive feedback stored")
	} else {
		thread.Feedback = -1
		thread.Update()

		profile.NegativeFeedbacks -= 1
		profile.Update()
		log.Println("negative feedback stored")
	}

	if self.config.OnThreadFeedback != nil {
		self.config.OnThreadFeedback(req.Feedback, profile, thread)
	}
}

func (self *NetApp) telephone_endpoint(thread *Thread) string {
	return fmt.Sprintf("%s,,,,%d", self.config.TelephoneNumber, thread.Otp)
}

func (self *NetApp) twilio_request(c web.C, w http.ResponseWriter, r *http.Request) {
	url_query := r.URL.Query()

	var otp uint64
	var err error

	from := url_query.Get("From")
	otp_text := url_query.Get("Otp")
	digits_text := url_query.Get("Digits")

	var error_response = func(code int) {
		body := `<?xml version="1.0" encoding="UTF-8"?>
<Response>
    <Reject reason="busy" />
</Response>`
		w.Header().Set("Content-Type", "application/xml")
		w.WriteHeader(code)
		w.Write([]byte(body))
	}

	var from_number string

	// `from` can be a client name...
	if strings.HasPrefix(from, "client:") {
		splitted := strings.SplitN(from, ":", 2)
		session := SessionByClientName(splitted[1])
		if session == nil {
			log.Printf("[twilio] malformed request. from=%s\n", from)
			error_response(400)
			return
		}
		from_number = session.Telephone
	} else {
		from_number = from
	}

	var thread *Thread

	if otp_text != "" || digits_text != "" {
		// We have an OTP (otp_text from a twilio client, digits_text from a
		// conventional telephone)
		if otp_text != "" {
			otp, err = strconv.ParseUint(otp_text, 10, 32)
		} else {
			otp, err = strconv.ParseUint(digits_text, 10, 32)
		}
		if err != nil {
			log.Println(err)
			log.Printf("[twilio] malformed request. from=%s otp_text=%s digits_text=%s\n", from, otp_text, digits_text)
			error_response(400)
			return
		}
		log.Printf("[twilio] callback from=%s otp=%d\n", from, otp)
		thread = ThreadByOTP(from_number, uint32(otp))
	} else {
		// if both the otp and the digits are empty it's possible that the
		// client is unable to send us the DTMF. As a workaround I can lookup
		// the pending_threads to see if we have something there.
		thread_id, ok := self.pending_threads[from_number]
		if !ok {
			// nothing to do, give up.
			log.Printf("[twilio] request without otp and from an unknown number. from=%s\n", from)
			error_response(400)
			return
		} else {
			log.Printf("[twilio] callback from=%s without an otp\n", from)
			thread = ThreadById(thread_id)
			delete(self.pending_threads, from_number)
		}
	}

	if thread == nil {
		log.Printf("[twilio] thread not found. from=%s otp=%d\n", from, otp)
		error_response(200)
		return
	}

	log.Printf("[twilio] thread found. from=%s otp=%d -> %s\n", from, otp, thread.Telephone2)

	var body string
	session := BestSessionByTelephoneNumber(thread.Telephone2)
	if session == nil {
		log.Printf("[twilio] internal inconsistency. thread found, session not found\n")
		error_response(400)
		return
	}

	var endpoint string
	callback_url := self.reachable_url(
		fmt.Sprintf("/v1beta/twilio_callback?thread=%d&amp;session_type=%s", thread.Id, session.Kind))

	if session.Kind == "physical" {
		body = `<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Dial timeLimit="60" callerId="%s" action="%s">
		<Number>%s</Number>
	</Dial>
</Response>`
		endpoint = thread.Telephone2
	} else {
		body = `<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Dial timeLimit="60" callerId="%s" action="%s">
		<Client>%s</Client>
	</Dial>
</Response>`
		endpoint = session.Client
	}
	log.Printf("[twilio] thread other endpoint %s\n", endpoint)
	body = fmt.Sprintf(body, self.config.TelephoneNumber, callback_url, endpoint)

	w.Header().Set("Content-Type", "application/xml")
	w.Write([]byte(body))
}

// twilio_callback is called by Twilio when a call ends
func (self *NetApp) twilio_callback(c web.C, w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		log.Printf("[twilio] error parsing the form from twilio furing the callback. err=%s", err)
		w.WriteHeader(400)
		return
	}

	url_query := r.URL.Query()
	call_status := r.PostForm.Get("DialCallStatus")
	call_sid := r.PostForm.Get("DialCallSid")
	var call_duration uint64
	if r.PostForm.Get("DialCallDuration") != "" {
		call_duration, err = strconv.ParseUint(r.PostForm.Get("DialCallDuration"), 10, 32)
	}
	if err != nil {
		log.Printf("[twilio] callback with an invalid call duration. err=%s\n", err)
		w.WriteHeader(400)
		return
	}

	thread_id, err := strconv.ParseInt(url_query.Get("thread"), 10, 64)
	if err != nil {
		log.Printf("[twilio] callback with an invalid thread id. thread=%s err=%s\n", url_query.Get("thread"), err)
		w.WriteHeader(400)
		return
	}

	session_type := url_query.Get("session_type")
	if session_type != "physical" && session_type != "web" {
		log.Printf("[twilio] callback with an invalid session type. session_type=%s\n", session_type)
		w.WriteHeader(400)
		return
	}

	thread := ThreadById(thread_id)
	if thread == nil {
		log.Printf("[twilio] callback with an invalid thread id. thread=%s", thread_id)
		http.NotFound(w, r)
		return
	}

	log.Printf("[twilio] callback status=%s sid=%s duration=%d thread_id=%d session_type=%s\n",
		call_status, call_sid, call_duration, thread.Id, session_type)
	if self.config.OnTwilioCallback != nil {
		self.config.OnTwilioCallback(call_status, call_sid, call_duration, thread, session_type)
	}

	body := `<?xml version="1.0" encoding="UTF-8"?>
<Response>
    <Hangup/>
</Response>`
	w.Header().Set("Content-Type", "application/xml")
	w.Write([]byte(body))
}

func NewWebMux(cfg NetConfig) *web.Mux {
	mux := web.New()

	cfg.PublicURL = strings.TrimRight(cfg.PublicURL, "/")
	app := NetApp{
		config:          cfg,
		pending_threads: make(map[string]int64),
	}
	mux.Post("/v1beta/me/sessions/", app.create_new_session)
	mux.Post("/v1beta/me/sessions/:session_id", app.validate_session)
	mux.Head("/v1beta/me", UserSession(app.session_keepalive))
	mux.Get("/v1beta/me", UserSession(app.user_profile))
	mux.Put("/v1beta/me", UserSession(app.set_user_profile))
	mux.Get("/v1beta/threads/", UserSession(app.list_threads))
	mux.Post("/v1beta/threads/", UserSession(app.create_new_thread))
	mux.Put("/v1beta/threads/:thread_id", UserSession(app.feedback_thread))
	mux.Handle("/v1beta/twilio_request", app.twilio_request)
	mux.Handle("/v1beta/twilio_callback", app.twilio_callback)

	// EnvInit should be the first middleware in the stack, as it makes
	// reasonably sure that the environment map will not be nil.
	mux.Use(middleware.EnvInit)
	mux.Use(cors)
	return mux
}

func NewLoggingWebMux(cfg NetConfig) *web.Mux {
	mux := NewWebMux(cfg)
	mux.Use(middleware.Logger)
	return mux
}
