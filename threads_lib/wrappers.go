package threads

import (
	"encoding/base64"
	"net/http"
	"strconv"
	"strings"

	"github.com/zenazn/goji/web"
)

func UserSession(f web.HandlerFunc) web.HandlerFunc {
	auth_error := func(w http.ResponseWriter) {
		w.Header().Set("WWW-Authenticate", "Basic realm=\"threads\"")
		http.Error(w, "Authorization failed", http.StatusUnauthorized)
	}
	fn := func(c web.C, w http.ResponseWriter, r *http.Request) {
		auth_header, ok := r.Header["Authorization"]
		if !ok {
			auth_error(w)
			return
		}

		auth := strings.SplitN(auth_header[0], " ", 2)
		if len(auth) != 2 || auth[0] != "Basic" {
			auth_error(w)
			return
		}

		payload, err := base64.StdEncoding.DecodeString(auth[1])
		if err != nil {
			auth_error(w)
			return
		}

		pair := strings.SplitN(string(payload), ":", 2)
		if len(pair) != 2 {
			auth_error(w)
			return
		}

		auth_code, err := strconv.ParseInt(pair[1], 10, 64)
		if err != nil {
			auth_error(w)
			return
		}

		session := SessionByAuthCode(auth_code)
		if session == nil {
			auth_error(w)
			return
		}

		session.Ping()
		session.Update()
		r.Header.Add("X-Caller-Telephone", session.Telephone)
		c.Env["Session"] = session

		f(c, w, r)
	}
	return fn
}

func cors(c *web.C, h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		headers := w.Header()
		if r.Method == "OPTIONS" {
			headers.Set("Access-Control-Allow-Origin", "*")
			headers.Set("Access-Control-Allow-Methods", "GET,POST,PUT,HEAD")
			headers.Set("Access-Control-Allow-Headers", "Accept,Authorization,Origin,Content-Type")
			return
		}
		headers.Set("Access-Control-Allow-Origin", "*")
		headers.Set("Access-Control-Expose-Headers", "Accept,Authorization,Origin,Content-Type")
		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
