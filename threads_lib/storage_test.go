package threads

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"
)

func TestLoadSave(t *testing.T) {

	temp_storage, err := ioutil.TempFile("", "")
	if err != nil {
		log.Panicln(err)
	}

	InitStorage(temp_storage)

	// Write two sessions to document. The second write is supposed to trigger
	// a bug related to file pointer reset
	_ = NewSession("+393381945799", "physical")
	_ = NewSession("+393381945801", "physical")

	b, err := ioutil.ReadFile(temp_storage.Name())
	if err != nil {
		t.Error("couldn't read file " + temp_storage.Name())
	}

	if b[0] != '{' {
		t.Error("garbage at beginning of file " + temp_storage.Name())
	}

	// If everything went fine, we should now have two sessions serialized in
	// the sessions file, including authcode and smscode

	if !strings.Contains(string(b), "\"smscode\":") {
		t.Error("smscode wasn't found")
	}
	if !strings.Contains(string(b), "\"authcode\":") {
		t.Error("authcode wasn't found")
	}

	// Now clear the storage
	ClearStorage()

	// At this point, the file is expected to be logically empty (it shouldn't
	// contain any structure). Let's re-load it to check
	InitStorage(temp_storage)

	if len(_threads) != 0 {
		t.Error("no threads were expected")
	}

	if len(_sessions) != 0 {
		t.Error("no sessions were expected")
	}

	os.Remove(temp_storage.Name())
}
