package threads

import (
	"math"
	"math/rand"
	"strconv"
)

// NewOTP generates a code composed by `n` digits, which can be used for OTP
// purposes.
func NewOTP(n uint32) uint32 {
	floor := uint32(math.Pow(10, float64(n-1)))
	ceil := uint32(math.Pow(10, float64(n)) - 1)

	// This makes sure the SMS code is always n digits long
	return floor + rand.Uint32()%(ceil-floor)
}

func NewUnguessableId() int64 {
	return rand.Int63()
}

func NewUngussableClientName() string {
	return "Client_" + strconv.FormatInt(NewUnguessableId(), 10)
}
