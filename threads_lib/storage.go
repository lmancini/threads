package threads

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

var _storage *os.File
var _threads []Thread
var _sessions []Session
var _profiles []Profile

type _json_document struct {
	Threads  []Thread
	Sessions []Session
	Profiles []Profile
}

// InitStorage initializes the storage subsystem; the function signature and
// the meaning of the arguments depends on the underlying technology used to
// persist our data structures.
// ATM we need an os.File where to store our data as a json document
func InitStorage(f *os.File) {
	_storage = f
	load()
}

// ClearStorage removes all the data resulting in an empty storage
func ClearStorage() {
	if _storage == nil {
		log.Panicln("storage not initialized")
	}
	_threads = make([]Thread, 0)
	_sessions = make([]Session, 0)
	_profiles = make([]Profile, 0)
	save()
}

func load() {
	if _storage == nil {
		log.Panicln("storage not initialized")
	}

	_, err := _storage.Seek(0, 0)
	if err != nil {
		log.Panicln(err)
	}

	bytes, err := ioutil.ReadAll(_storage)
	if err != nil {
		log.Panicln(err)
	}

	doc := _json_document{}
	json.Unmarshal(bytes, &doc)
	_threads = doc.Threads
	_sessions = doc.Sessions
	_profiles = doc.Profiles

	log.Println("loaded " + strconv.Itoa(len(_threads)) + " threads")
	log.Println("loaded " + strconv.Itoa(len(_sessions)) + " sessions")
	log.Println("loaded " + strconv.Itoa(len(_profiles)) + " profiles")
}

func save() {
	if _storage == nil {
		log.Panicln("storage not initialized")
	}
	doc := _json_document{
		Threads:  _threads,
		Sessions: _sessions,
		Profiles: _profiles,
	}

	bytes, err := json.MarshalIndent(doc, "", "    ")
	if err != nil {
		log.Panicln(err)
	}

	err = _storage.Truncate(0)
	if err != nil {
		log.Panicln(err)
	}

	_, err = _storage.Seek(0, 0)
	if err != nil {
		log.Panicln(err)
	}

	_, err = _storage.Write(bytes)
	if err != nil {
		log.Panicln(err)
	}
}
