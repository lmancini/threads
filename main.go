package main

import (
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	threads "bitbucket.org/lmancini/threads/threads_lib"
)

var Settings struct {
	PublicURL   string
	Address     string
	Telephone   string
	PlivoId     string
	PlivoToken  string
	ParseAppId  string
	ParseApiKey string
	//TwilioSID   string
	//TwilioToken string
}

func LoadFromEnv() {
	Settings.PublicURL = os.Getenv("THREADS_PUBLIC_URL")
	if Settings.PublicURL == "" {
		log.Fatalln("THREADS_PUBLIC_URL not set")
	}

	Settings.Address = os.Getenv("THREADS_ADDR")
	if Settings.Address == "" {
		Settings.Address = ":9000"
	}

	Settings.Telephone = os.Getenv("THREADS_NUMBER")
	if Settings.Telephone == "" {
		log.Fatalln("THREADS_NUMBER not set")
	}

	/*
		Settings.TwilioSID = os.Getenv("TWILIO_SID")
		if Settings.TwilioSID == "" {
			log.Fatalln("TWILIO_SID not set")
		}

		Settings.TwilioToken = os.Getenv("TWILIO_TOKEN")
		if Settings.TwilioToken == "" {
			log.Fatalln("TWILIO_TOKEN not set")
		}
	*/

	Settings.PlivoId = os.Getenv("PLIVO_ID")
	Settings.PlivoToken = os.Getenv("PLIVO_TOKEN")
	if Settings.PlivoId == "" || Settings.PlivoToken == "" {
		log.Println("Warning: Plivo carrier for SMS is not configured")
	}

	Settings.ParseAppId = os.Getenv("PARSE_APP_ID")
	Settings.ParseApiKey = os.Getenv("PARSE_API_KEY")
	if Settings.ParseAppId == "" || Settings.ParseApiKey == "" {
		log.Println("Warning: Parse backend for push notifications is not configured")
	}
}

func main() {
	LoadFromEnv()

	rand.Seed(time.Now().UTC().UnixNano())

	log.Println("Initializing storage")
	storage, err := os.OpenFile("db.json", os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Panicln(err)
	}
	threads.InitStorage(storage)

	log.Printf("Server starts: listen=%s reachable=%s number=%s\n", Settings.Address, Settings.PublicURL, Settings.Telephone)

	// Initialize SMS gateway
	var sms_gateway threads.SmsGateway
	if Settings.PlivoId != "" && Settings.PlivoToken != "" {
		sms_gateway = &PlivoGateway{Settings.PlivoId, Settings.PlivoToken}
		log.Println("sms gateway: plivo")
	} else {
		sms_gateway = &LogGateway{}
		log.Println("sms gateway: console log")
	}

	// Initialize push notifications gateway
	var push_gateway threads.PushGateway
	if Settings.ParseAppId != "" && Settings.ParseApiKey != "" {
		push_gateway = &ParsePushGateway{Settings.ParseAppId, Settings.ParseApiKey}
		log.Println("push gateway: parse")
	} else {
		push_gateway = &LogPushGateway{}
		log.Println("push gateway: console log")
	}

	cfg := threads.NetConfig{
		PublicURL:       Settings.PublicURL,
		TelephoneNumber: Settings.Telephone,
		SMS:             sms_gateway,
		Push:            push_gateway,
	}
	mux := threads.NewLoggingWebMux(cfg)
	server := http.Server{Handler: mux, Addr: Settings.Address}

	log.Fatal(server.ListenAndServe())
}
