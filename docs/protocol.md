# Thread http protocol

## Client

Un client di `Thread` **deve** seguire i redirect inviati dal server.

**RATIONALE:**

> Questo requisito rende possibili alcuni scenari interessanti.
>
> Ad esempio potremmo pubblicare una versione preliminare dell'api sotto la
> radice /v1beta/ e quando decidiamo di rilasciare ufficialmente la versione
> 1, esposta sotto /v1/, possiamo far migrare tutti i client che attualmente
> utilizzano l'api in beta senza aspettare la loro riscrittura.
>
> Un altro scenario è il dirottamento di specifici client (di sviluppo? con
> bug particolari?) verso risorse ad hoc senza per questo cambiare l'api per
> tutti quanti; ad esempio se volessimo testare sul campo una nuova feature
> potremmo fare un roll-out progressivo in base al client (tutti i client il
> cui numero di telefono finisce per X fanno un redirect verso la nuova
> feature).

## Autenticazione

Tutte le risorse esposte dal protocollo, ad eccezione di quelle marcate come
**ANONYMOUS**, richiedono che il client si autentichi.

Un client si autentica con l'header `Authorization` utilizzando **Basic** come
tipo di autenticazione e costruendo le credenziali utilizzando la stringa vuota
come **username** e l'auth_code (vedi `sessions`) come **password**.

## Content-Type

Il body di tutte le richieste e di tutte le risposte è un documento json, ad
eccezione di quelle marcate esplicitamente come xml.

## Date

Le date e gli orari devono essere formattati secondo [iso8601](https://en.wikipedia.org/wiki/ISO_8601).

## Numeri di telefono

I numeri di telefono devono essere formattati secondo [E.164](https://en.wikipedia.org/wiki/E.164).

## Protocollo version 1-beta

### Sessione

Per ottenere un `auth_code` e poter accedere alle risorse che richiedono
autenticazione è necessario registrarsi eseguendo una `POST` sull'endpoint
`/sessions/`.

Questo endpoint restituisce un `session_id` che dovrà essere convertito insieme
al codice che verrà inviato al telefono tramite SMS nell'`auth_code`.

Il numero di telefono è dunque *sempre necessario* anche se si registra un client web.

L'`auth code` non ha una scadenza definita, il client può memorizzarlo nel modo
che crede (ovviamente dato che impersona in tutto e per tutto l'utente deve
essere considerato un segreto).

Se è necessario ottenere un nuovo `auth_code` bisogna ripetere il flusso di
registrazione (e quindi avere a diposizione il telefono per ricevere il nuovo
codice via SMS).

`POST /v1beta/me/sessions/` `[ANONYMOUS]`

REQUEST:

    {
        "telephone": "",
        "kind": ""
    }

RESPONSE:

    {
        "session_id": "",
        "client": ""
    }

Registra un nuovo client, che può essere di tipo `kind` ("physical" o "web"),
utilizzando il numero di telefono passato nel body della richiesta.

La risposta dal server contiene sia il `session_id` (descritto nella sezione
`Sessioni`) che un nome univoco per il client. Questo nome è particolarmente
importante per i client WebRTC che lo devono utilizzare per registrarsi su
Twilio.


`POST /v1beta/me/sessions/<session_id>` `[ANONYMOUS]`

REQUEST:

    {
        "sms_code": ""
    }

RESPONSE:

    {
        "auth_code": ""
    }

Valida il session_id e il codice ricevuto via SMS e ritorna un `auth_code`.

`GET /v1beta/me/sessions/` `[OPTIONAL]`

RESPONSE:

    [
        {
            "client": "",
            "last_seen": ""
        }
    ]

Elenca tutte le sessioni conosciute dal backend

### Profilo

`GET /v1beta/me`

RESPONSE:

    {
        "telephone": "",
        "data": {},
        "session": {
            "client": ""
        }
    }

Recupera il profilo dell'utente corrente. Il profilo è diviso in tre sezioni:

    - identificativo telefonico
    - dati del profilo
    - dati della sessione corrente

I dati sulla sessione corrente contengono solo il nome univoco del client che
ha eseguito la richiesta.

I dati sul profilo possono contenere informazioni diverse a seconda
dell'installazione utilizzata (rendez-vous, whitelabel)

`PUT /v1beta/me`

REQUEST:

    {
        "key": "value"
    }

Modifica la sezione `date del profilo` (vedi GET) dell'utente corrente. Il
profilo passato tramite la `PUT` può essere parziale, vengono modificati i soli
campi presenti nella richiesta.

Il numero di telefono **non può essere modificato**.

`HEAD /v1beta/me`

Richiesta esplicita di Keep-alive sulla sessione corrente.

### Threads

`GET /v1beta/twilio_request` (XML)

RESPONSE:

    <?xml version="1.0" encoding="UTF-8"?>
    <Response>
        <Dial timeLimit="60" callerId="">
            <Number>""</Number>
        </Dial>
    </Response>

Questa richiesta è conforme al protocollo definito dal carrier Twilio ed è
effettuata da quest'ultimo per decidere cosa fare quando viene ricevuta una
chiamata. `callerId` è il numero che apparirà come chiamante all'utente
destinatario.  L'implementazione deve controllare il codice Dialer presente
nella request (deve essere lo stesso codice numerico codificato in `endpoint`)
ed in tal caso comunicare al carrier il `number` a cui vogliamo collegare il
chiamante.

`POST /v1beta/threads/`

RESPONSE:

    {
        "id": "",
        "endpoint": ""
    }

Crea un nuovo thread, l'id è una stringa identificativa mentre endpoint è il
numero di telefono da chiamare.

`PUT /v1beta/threads/<id>`

REQUEST:

    {
        "feedback": "-1"|"0"|"+1"
    }

Lascia un feedback sul thread appena concluso, il valori possono essere solo
quelli elencati.

Il valore "0" viene automaticamente assegnato dal server nel caso un feedback
non venga dato entro cinque minuti dal termine della chiamata.

Il valore "+1" invia al contatto del thread il campo "second_chance"
del profilo dell'utente.

`GET /v1beta/threads/` `[OPTIONAL]`

RESPONSE:

    [
        {
            "id": "",
            "timestamp": "",
            "feedback": ""
        }
    ]

Lo storico dei propri threads in ordine di timestamp decrescente.
