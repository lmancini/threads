package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type LogGateway struct{}

func (self *LogGateway) Send(from, to, msg string) error {
	log.Printf("new sms: %s -> %s \"%s\"\n", from, to, msg)
	return nil
}

func (self *LogGateway) SendVerificationCode(from, to string, code uint32) error {
	log.Printf("new sms code: %s -> %s code: %d\n", from, to, code)
	return nil
}

type PlivoGateway struct {
	AuthId    string
	AuthToken string
}

func (self *PlivoGateway) Send(from, to, msg string) error {
	log.Printf("new sms: %s -> %s \"%s\"\n", from, to, msg)

	var sms_data = make(map[string]string)
	sms_data["src"] = from
	sms_data["dst"] = to
	sms_data["text"] = msg

	bytes, err := json.Marshal(sms_data)
	if err != nil {
		log.Panicln(err)
	}

	err = self.post(bytes)
	if err != nil {
		log.Println("error sending an sms via plivo:", err)
	}
	return err
}

func (self *PlivoGateway) SendVerificationCode(from, to string, code uint32) error {
	msg := "Your Threads verification code is " + strconv.FormatInt(int64(code), 10)
	return self.Send(from, to, msg)
}

func (self *PlivoGateway) post(body []byte) error {
	url := "https://api.plivo.com/v1/Account/%s/Message/"
	url = fmt.Sprintf(url, self.AuthId)

	req, err := http.NewRequest("POST", url, bytes.NewReader(body))
	if err != nil {
		return err
	}
	req.SetBasicAuth(self.AuthId, self.AuthToken)
	req.Header.Set("Content-Type", "application/json")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	return nil
}
