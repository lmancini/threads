.PHONY: test threads

all: build;

build: threads;

threads:
	go build

test:
	go test --cover ./...

.ONESHELL:
SHELL = /bin/bash
run: build
	@source environ
	@./threads
	@true
